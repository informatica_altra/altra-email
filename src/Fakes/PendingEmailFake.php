<?php

namespace Altra\Email\Fakes;

use Altra\Email\Exceptions\ReceiverNotSetException;
use Altra\Email\Exceptions\TemplateNotSetException;
use Altra\Email\PendingEmail;
use PHPUnit\Framework\Assert as PHPUnit;

class PendingEmailFake extends PendingEmail
{
    /**
     * Emails sent
     *
     * @var array
     */
    protected $sentEmails = [];

    /**
     * Send a new message using a view.
     *
     * @param  \Illuminate\Contracts\Mail\Mailable|string|array  $view
     * @param  array  $data
     * @param  \Closure|string|null  $callback
     * @return void
     */
    public function send()
    {
        throw_if(! isset($this->template), new TemplateNotSetException());
        throw_if(count($this->to) == 0, new ReceiverNotSetException());
        $this->getTemplate();
        $this->sentEmails[] = $this->toArray();
    }

    public function assertSent($azIso)
    {
        $collection = collect($this->sentEmails);
        $message = "The expected [{$this->template}] template was not sent.";

        PHPUnit::assertTrue($collection->where('template', $azIso)->isNotEmpty(), $message);
    }

    public function assertSentTo($email, $name)
    {
        $collection = collect($this->sentEmails);
        $message = "The expected [{$this->template}] template was not sent to this user ($email, $name)";
        $condition = $collection->filter(function ($item) use ($email, $name) {
            $to = collect($item['to']);

            return $to->where('name', $name)->where('email', $email)->isNotEmpty();
        })->isNotEmpty();
        PHPUnit::assertTrue($condition, $message);
    }

    public function assertSentFrom($senderEmail)
    {
        $collection = collect($this->sentEmails);
        $message = "The expected [{$this->template}] template was not sent from this user ($senderEmail)";
        PHPUnit::assertTrue($collection->where('from', $senderEmail)->isNotEmpty(), $message);
    }

    public function assertSentFromName($senderName)
    {
        $collection = collect($this->sentEmails);
        $message = "The expected [{$this->template}] template was not sent from this user ($senderName)";
        PHPUnit::assertTrue($collection->where('fromName', $senderName)->isNotEmpty(), $message);
    }

    public function assertEmailSubject($subject)
    {
        $collection = collect($this->sentEmails);
        $message = "Subject doesn`t match ($subject)";
        PHPUnit::assertTrue($collection->where('subject', $subject)->isNotEmpty(), $message);
    }

    public function assertEmailMarket($iso)
    {
        $collection = collect($this->sentEmails);
        $message = "Market iso doens`t match ($iso)";
        PHPUnit::assertTrue($collection->where('market.iso', $iso)->isNotEmpty(), $message);
    }

    private function getTemplate()
    {
        $faker = fake();
        $this->templateModel = [
            'id' => 1,
            'az_iso' => $this->template,
            'type_iso' => 'LAYOUT_BASIC',
            'subject' => $faker->sentence(),
            'pre_header' => $faker->sentence(),
            'body_1' => $faker->text(),
        ];
    }
}
