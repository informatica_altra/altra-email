<?php

namespace Altra\Email;

use Altra\Email\Exceptions\NotifierException;
use Altra\Email\Exceptions\ReceiverNotSetException;
use Altra\Email\Exceptions\TemplateNotSetException;
use Illuminate\Support\Facades\Http;

class PendingEmail
{
    public function __construct()
    {
        if (!empty(auth()->user()->market_assigned)) {
            $this->market(auth()->user()->market_assigned);
        }
    }

    protected array $to = [];

    protected string $from = 'no-reply@airzonecontrol.com';

    protected string $fromName = 'Airzone Control';

    protected string $subject;

    protected string $reply = 'no-reply@airzonecontrol.com';

    protected string $template;

    protected array $templateModel = [];

    protected array $variables = [];

    protected array $headerVariables = [];

    protected array $subjectVariables = [];

    protected array $market;

    protected string $locale;

    public function to(string $email, string $name)
    {
        $this->to = collect($this->to)->push([
            'email' => config('altra_email.app_env') == 'production' ? $email : config('altra_email.test_mail'),
            'type' => 'to',
            'name' => $name
        ])->toArray();

        return $this;
    }

    public function from(string $from)
    {
        $this->from = $from;

        return $this;
    }

    public function fromName(string $fromName)
    {
        $this->fromName = $fromName;

        return $this;
    }

    public function subject(string $subject)
    {
        $this->subject = $subject;

        return $this;
    }

    public function reply(string $reply)
    {
        $this->reply = $reply;

        return $this;
    }

    public function template(string $template)
    {
        $this->template = $template;

        return $this;
    }

    public function variables(array $variables)
    {
        $this->variables = $variables;

        return $this;
    }

    public function headerVariables(array $headerVariables)
    {
        $this->headerVariables = $headerVariables;

        return $this;
    }

    public function subjectVariables(array $subjectVariables)
    {
        $this->subjectVariables = $subjectVariables;

        return $this;
    }

    public function market(array $market)
    {
        $this->market = $market;
        $this->locale = collect(explode('-', $this->market['default_locale']))->last();

        return $this;
    }

    public function locale(string $locale)
    {
        $this->locale = $locale;

        return $this;
    }

    public function send()
    {
        throw_if(! isset($this->template), new TemplateNotSetException());
        throw_if(count($this->to) == 0, new ReceiverNotSetException());
        // Bring Template From MsTemplates
        $this->getTemplate();
        $template = $this->templateModel['type_iso'];
        $notifier = env('MSNOTIFIER');
        $url = "$notifier/send/$template";

        $sent_result = Http::withHeaders([
            'Accept' => 'application/json',
            'Content-type' => 'application/json', ])
        ->post($url, $this->getData()->toArray());

        throw_if($sent_result->status() >= 400, new NotifierException($sent_result));

        return $sent_result;
    }

    private function getData()
    {
        $body = __($this->templateModel['body_1'], $this->variables);
        $header = __($this->templateModel['pre_header'], $this->headerVariables);
        $subject = __($this->templateModel['subject'], $this->subjectVariables);

        $globalVariables = collect([
            [
                'name' => 'PRE_HEADER',
                'content' => $header,
            ],
            [
                'name' => 'BODY',
                'content' => $body,
            ],
        ]);

        collect($this->market['social'])->map(function ($social) use ($globalVariables) {
            $globalVariables->push([
                'name' => $social['merge_tag'],
                'content' => $social['merge_tag_value'],
            ]);
        });

        /**
         * Añadimos el locale por defecto de mercado
         */
        $globalVariables->push([
            'name' => 'LOCALE',
            'content' => $this->locale,
        ]);

        /**
         * Añadimos el footer del mercado
         */
        $globalVariables->push([
            'name' => 'FOOTER',
            'content' => $this->market['email_footer'],
        ]);

        return collect([
            'subject' => $subject,
            'from_email' => $this->from,
            'from_name' => $this->fromName,
            'reply' => $this->reply,
            'to' => $this->to,
            'global_merge_vars' => $globalVariables,
        ]);
    }

    private function getTemplate()
    {
        $this->templateModel = Http::withHeaders([
            'Accept' => 'application/json',
            'Content-type' => 'application/json',
            'apikey' => env('KONG_API_KEY'),
            'app-locale' => $this->locale,
            'app-market' => $this->market['iso'],
        ])->get(env('MSDASHBOARD_URL').'.pv1/email-template/'.$this->template)['body']['email_template'];
    }

    public function toArray(): array
    {
        $properties = get_object_vars($this);
        $array = [];
        foreach ($properties as $property => $type) {
            $array[$property] = $this->$property;
        }

        return $array;
    }
}
