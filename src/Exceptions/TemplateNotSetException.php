<?php

namespace Altra\Email\Exceptions;

use Exception;

class TemplateNotSetException extends Exception
{
    public function __construct()
    {
        parent::__construct('You must choose one template', 500);
    }
}
