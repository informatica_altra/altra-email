<?php

namespace Altra\Email\Exceptions;

use Exception;

class ReceiverNotSetException extends Exception
{
    public function __construct()
    {
        parent::__construct('You must set at least one receiver', 500);
    }
}
