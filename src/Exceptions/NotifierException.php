<?php

namespace Altra\Email\Exceptions;

use Exception;

class NotifierException extends Exception
{
  public function __construct($notifierResponse)
  {

    $message = 'Error from notifier response';

    if ($notifierResponse->status() >= 400 && isset($notifierResponse->json()['error'])) {
      $message = $notifierResponse->json()['error'];
    }

    parent::__construct($message, $notifierResponse->status());
  }
}
