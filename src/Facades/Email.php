<?php

namespace Altra\Email\Facades;

use Altra\Email\Fakes\PendingEmailFake;
use Altra\Email\PendingEmail;
use Illuminate\Support\Facades\Facade;

class Email extends Facade
{
    protected static function getFacadeAccessor()
    {
        return PendingEmail::class;
    }

    /**
     * Replace the bound instance with a fake.
     *
     * @return Altra\Email\Fakes\PendingEmailFake
     */
    public static function fake()
    {
        static::swap($fake = new PendingEmailFake);

        return $fake;
    }
}
