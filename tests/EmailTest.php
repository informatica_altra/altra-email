<?php

use Altra\Email\Exceptions\ReceiverNotSetException;
use Altra\Email\Exceptions\TemplateNotSetException;
use Altra\Email\Facades\Email;
use Altra\Email\Tests\TestCase;

class EmailTest extends TestCase
{
  public function setUp(): void
  {
    parent::setUp();

    Email::fake();
  }

  public function test_email_was_sent()
  {
    Email::to('test@test.test', 'Test')->template('AZ_ISO')->send();

    Email::assertSent('AZ_ISO');
  }

  public function test_template_not_set_exception()
  {
    $this->expectException(TemplateNotSetException::class);
    Email::to('test@test.test', 'Test')
      ->send();
  }

  public function test_to_not_set_exception()
  {
    $this->expectException(ReceiverNotSetException::class);
    Email::template('AZ_ISO')
      ->send();
  }

  public function test_email_sender()
  {
    Email::to('test@test.test', 'Test')
      ->template('AZ_ISO')
      ->fromName('Test')
      ->send();

    Email::assertSentFromName('Test');
  }

  public function test_email_subject()
  {
    Email::to('test@test.test', 'Test')
      ->template('AZ_ISO')
      ->subject('Subject')
      ->send();

    Email::assertEmailSubject('Subject');
  }

  public function test_email_market()
  {
    Email::to('test@test.test', 'Test')
      ->template('AZ_ISO')
      ->market([
        'iso'            => 'EU',
        'default_locale' => 'en',
      ])
      ->send();

    Email::assertEmailMarket('EU');
  }
}
