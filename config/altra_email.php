<?php
return [
    'test_mail' => env('TEST_MAIL'),
    'app_env' => env('APP_ENV')
];